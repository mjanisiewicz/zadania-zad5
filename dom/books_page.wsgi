#!/usr/bin/python
# -*- coding=utf-8 -*-


import sha, time, cgi, os
import urlparse

class BookDB():
    def titles(self):
        titles = [dict(id=id, title=database[id]['title']) for id in database.keys()]
        return titles

    def title_info(self, id):
        return database[id]


database = {
    'id1': {'title': 'CherryPy Essentials: Rapid Python Web Application Development',
            'isbn': '978-1904811848',
            'publisher': 'Packt Publishing (March 31, 2007)',
            'author': 'Sylvain Hellegouarch',
            },
    'id2': {'title': 'Python for Software Design: How to Think Like a Computer Scientist',
            'isbn': '978-0521725965',
            'publisher': 'Cambridge University Press; 1 edition (March 16, 2009)',
            'author': 'Allen B. Downey',
            },
    'id3': {'title': 'Foundations of Python Network Programming',
            'isbn': '978-1430230038',
            'publisher': 'Apress; 2 edition (December 21, 2010)',
            'author': 'John Goerzen',
            },
    'id4': {'title': 'Python Cookbook, Second Edition',
            'isbn': '978-0-596-00797-3',
            'publisher': 'O''Reilly Media',
            'author': 'Alex Martelli, Anna Ravenscroft, David Ascher',
            },
    'id5': {'title': 'The Pragmatic Programmer: From Journeyman to Master',
            'isbn': '978-0201616224',
            'publisher': 'Addison-Wesley Professional (October 30, 1999)',
            'author': 'Andrew Hunt, David Thomas',
            },
    }


def application(environ, start_response):
    books = BookDB()
    query = urlparse.parse_qs(environ.get('QUERY_STRING', 'UNKNOWN'))
    response_body = '<html><head><meta charset="utf-8" /><title>Biblioteka: </title></head><body>'
    titles = books.titles()
    id = query.get('bookid', [''])[0]
    if id == "":
        response_body += '<h3>Biblioteka: </h3>'
        response_body += '<ul>'
        for elem in titles:
            response_body += "<li><a href='?bookid=" + elem['id'] + "'>" + elem['title'] + "</a></li>"
        response_body += "</ul></body></html>"
    else:
        try:
            book_item = books.title_info(id)
            book = """<div>Tytuł:</div><div style="margin-left: 20px">%s.</div>
                    <div>Autor:</div><div style="margin-left: 20px">%s.</div>
                    <div>Wydawca:</div><div style="margin-left: 20px">%s.</div>
                    <div>NR isbn:</div><div style="margin-left: 20px">%s.</div>"""
            book = book % (
                book_item['title'],
                book_item['author'],
                book_item['publisher'],
                book_item['isbn'],
            )
            response_body += book
        except:
            response_body += 'Brak danej pozycji'
        response_body += "</body></html>"
    status = '200 OK'
    response_headers = [('Content-Type', 'text/html'),
                        ('Content-Length', str(len(response_body)))]
    start_response(status, response_headers)
    return [response_body]


#if __name__ == '__main__':
#    from wsgiref.simple_server import make_server

#    app = Session(Upperware(application))

#    srv = make_server('', 31015, app)
#    srv.serve_forever()

#    from wsgiref.simple_server import make_server
#    srv = make_server('localhost', 4444, application)
#    srv.serve_forever()

